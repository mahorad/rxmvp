package android.volvo.com.sample.panel.list;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.volvo.com.sample.panel.list.mvp.ItemListPresenter;
import android.volvo.com.sample.panel.list.mvp.ItemListView;

public class ItemListActivity extends AppCompatActivity {

    private ItemListView view;
    private ItemListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        view = new ItemListView(this);
        presenter = new ItemListPresenter(view);

        setContentView(view.getView());
        presenter.onCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

}
