package android.volvo.com.sample.panel.detail.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.volvo.com.sample.R;
import android.volvo.com.sample.panel.detail.ItemDetailActivity;
import android.volvo.com.sample.panel.detail.ItemDetailFragment;
import android.volvo.com.sample.panel.list.ItemListActivity;
import android.widget.FrameLayout;

import com.jakewharton.rxbinding2.view.RxView;

import io.reactivex.Observable;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class ItemDetailView extends FrameLayout {

    //    @BindView(R.id.detail_toolbar)
    Toolbar toolbar;

    //    @BindView(R.id.fab)
    FloatingActionButton fab;

    public ItemDetailView(ItemDetailActivity activity) {
        super(activity);

        setLayoutParams(new ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        inflate(getContext(), R.layout.activity_item_detail, this);

        toolbar = findViewById(R.id.detail_toolbar);
        activity.setSupportActionBar(toolbar);

        fab = findViewById(R.id.fab);

        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ItemDetailFragment fragment = new ItemDetailFragment();
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.item_detail_container, fragment)
                .commit();

    }

    public Observable<Object> observableFloatingButton() {
        return RxView.clicks(fab);
    }

    public void showMessage(String message) {
        Snackbar.make(fab, message, Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show();
    }

    public View getView() {
        return this;
    }
}
