package android.volvo.com.sample.panel.list.mvp;


import android.volvo.com.sample.panel.model.Item;
import android.volvo.com.sample.panel.model.Model;

import io.reactivex.Observable;

public class ItemListModel {

    public int getLastPosition() {
        return getTotalItems() - 1;
    }

    public Observable<Item> getItemsObservable() {
        return Model.getObservable();
    }

    public Item getItemByPosition(int position) {
        return Model.getByPosition(position);
    }

    public int getTotalItems() {
        return Model.getSize();
    }

    public void setSelected(String itemId) {
        Item item = Model.getById(itemId);
        Model.setSelected(item);
    }
}
