package android.volvo.com.sample.panel.list.mvp;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.volvo.com.sample.R;
import android.volvo.com.sample.panel.detail.ItemDetailActivity;
import android.volvo.com.sample.panel.detail.ItemDetailFragment;
import android.widget.TextView;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class ItemListPresenter {

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final ItemListView view;
    private final ItemListModel model = new ItemListModel();

    public ItemListPresenter(ItemListView view) {
        this.view = view;
    }

    public void onCreate() {
        compositeDisposable.add(subscribeFloatingButton());
        compositeDisposable.add(subscribeAdapterViewHolder());
        compositeDisposable.add(subscribeItemListChanges());
    }

    private Disposable subscribeFloatingButton() {
        return view
                .observableFloatingButton()
                .subscribe(o -> view.showMessage("Replace with your own action please"));
    }

    private Disposable subscribeAdapterViewHolder() {
        return view
                .observableAdapterViewHolder()
                .subscribe(this::showDetails);
    }

    private Disposable subscribeItemListChanges() {
        return model
                .getItemsObservable()
                .subscribe(item -> view.notifyItemAddedAt(model.getLastPosition()));
    }

    private void showDetails(View view) {
        TextView idView = view.findViewById(R.id.id);
        String itemId = idView.getTag().toString();
        model.setSelected(itemId);
        if (this.view.isLargeScreen()) {
            this.view.updateDetailsFragment();
        } else {
            startDetailsActivity();
        }
    }

    private void startDetailsActivity() {
        Context context = view.getContext();
        Intent intent = new Intent(context, ItemDetailActivity.class);
        context.startActivity(intent);
    }

    public void onDestroy() {
        compositeDisposable.clear();
    }
}
