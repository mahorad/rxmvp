package android.volvo.com.sample.panel.list.mvp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.volvo.com.sample.R;
import android.volvo.com.sample.panel.detail.ItemDetailFragment;
import android.volvo.com.sample.panel.list.ItemListActivity;
import android.widget.FrameLayout;

import com.jakewharton.rxbinding2.view.RxView;

import io.reactivex.Observable;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class ItemListView extends FrameLayout {

//    @BindView(R.id.toolbar)
    Toolbar toolbar;

//    @BindView(R.id.fab)
    FloatingActionButton fab;

//    @BindView(R.id.item_list)
    RecyclerView recyclerView;

    ItemRecyclerAdapter adapter;
    FragmentManager fragman;

    public ItemListView(ItemListActivity activity) {
        super(activity);

        setLayoutParams(new ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        inflate(getContext(), R.layout.activity_item_list, this);
//        ButterKnife.bind(this);

        toolbar = findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);
        toolbar.setTitle("items");

        fab = findViewById(R.id.fab);

        recyclerView = findViewById(R.id.item_list);
        fragman = activity.getSupportFragmentManager();
        adapter = new ItemRecyclerAdapter();
        recyclerView.setAdapter(adapter);

    }

    public void notifyItemAddedAt(int position) {
        adapter.notifyItemInserted(position);
    }

    public Observable<View> observableAdapterViewHolder() {
        return adapter.getViewClickedObservable();
    }

    public Observable<Object> observableFloatingButton() {
        return RxView.clicks(fab);
    }

    public View getView() {
        return this;
    }

    public void showMessage(String message) {
        Snackbar.make(fab, message, Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show();
    }

    public void updateDetailsFragment() {
        ItemDetailFragment fragment = new ItemDetailFragment();
        fragman
                .beginTransaction()
                .replace(R.id.item_detail_container, fragment)
                .commit();
    }

    public boolean isLargeScreen() {
        return findViewById(R.id.item_detail_container) != null;
    }
}
