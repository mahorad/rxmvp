package android.volvo.com.sample.panel.detail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.volvo.com.sample.panel.detail.mvp.ItemDetailPresenter;
import android.volvo.com.sample.panel.detail.mvp.ItemDetailView;
import android.volvo.com.sample.panel.list.ItemListActivity;

public class ItemDetailActivity extends AppCompatActivity {

    ItemDetailView view;
    ItemDetailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        view = new ItemDetailView(this);
        presenter = new ItemDetailPresenter(view);

        setContentView(view.getView());

        presenter.onCreate();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, ItemListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
