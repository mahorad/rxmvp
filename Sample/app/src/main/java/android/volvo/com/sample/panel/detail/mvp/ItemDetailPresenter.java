package android.volvo.com.sample.panel.detail.mvp;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class ItemDetailPresenter {

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final ItemDetailView view;
    private final ItemDetailModel model = new ItemDetailModel();

    public ItemDetailPresenter(ItemDetailView view) {
        this.view = view;
    }

    public void onCreate() {
        compositeDisposable.add(subscribeFloatingButton());
    }

    private Disposable subscribeFloatingButton() {
        return view
                .observableFloatingButton()
                .subscribe(o -> view.showMessage("You need to replace this with your own action"));
    }

    public void onDestroy() {
        compositeDisposable.clear();
    }
}
