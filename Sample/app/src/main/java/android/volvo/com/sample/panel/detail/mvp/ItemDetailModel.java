package android.volvo.com.sample.panel.detail.mvp;

import android.volvo.com.sample.panel.model.Item;
import android.volvo.com.sample.panel.model.Model;

public class ItemDetailModel {

    public Item getItemById(String id) {
        return Model.getById(id);
    }

    public Item getSelectedItem() {
        return Model.getSelected();
    }
}
