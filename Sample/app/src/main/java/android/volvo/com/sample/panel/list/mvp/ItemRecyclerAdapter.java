package android.volvo.com.sample.panel.list.mvp;

import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.volvo.com.sample.R;
import android.volvo.com.sample.panel.model.Item;

import com.jakewharton.rxbinding2.view.RxView;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class ItemRecyclerAdapter extends Adapter<ItemRecyclerViewHolder> {

    private PublishSubject<View> mViewClickSubject = PublishSubject.create();

    // TODO inject it later
    ItemListModel model = new ItemListModel();

    public Observable<View> getViewClickedObservable() {
        return mViewClickSubject.hide();
    }

    @Override
    public ItemRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_list_content, parent, false);

        ItemRecyclerViewHolder itemRecyclerViewHolder = new ItemRecyclerViewHolder(view);

        RxView.clicks(view)
                .takeUntil(RxView.detaches(parent))
                .map(aVoid -> view)
                .subscribe(mViewClickSubject);

        return itemRecyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemRecyclerViewHolder holder, int position) {
        Item item = model.getItemByPosition(position);
        holder.mItem = item;
        holder.mIdView.setText(item.id);
        holder.mIdView.setTag(item.id);
        holder.mContentView.setText(item.content);
    }

    @Override
    public int getItemCount() {
        return model.getTotalItems();
    }

}
