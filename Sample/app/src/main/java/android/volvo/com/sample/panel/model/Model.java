package android.volvo.com.sample.panel.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class Model {

    private static final List<Item> items = new ArrayList<Item>();

    private static final Map<String, Item> itemsMap = new HashMap<String, Item>();

    private static Item selected;

    private static final int COUNT = 3;

    static {
        for (int i = 1; i <= COUNT; i++) {
            addItem(createItem(i));
        }
    }

    public static Observable<Item> getObservable() {
        return Observable
                .interval(5000L, MILLISECONDS)
                .timeInterval()
                .take(27)
                .observeOn(AndroidSchedulers.mainThread())
                .map(i -> {
                    int position = COUNT + i.value().intValue() + 1;
                    Item item = createItem(position);
                    addItem(item);
                    return item;
                });
    }

    private static void addItem(Item item) {
        items.add(item);
        itemsMap.put(item.id, item);
    }

    private static Item createItem(int position) {
        return new Item(String.valueOf(position), "Item " + position, makeDetails(position));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position + "\n");
        for (int i = 0; i < position; i++) {
            builder.append("\ndetails ");
        }
        return builder.toString();
    }

    public static Item getById(String itemId) {
        return itemsMap.get(itemId);
    }

    public static Item getByPosition(int index) {
        return items.get(index);
    }

    public static int getSize() {
        return items.size();
    }

    public static Item getSelected() {
        return selected;
    }

    public static void setSelected(Item item) {
        selected = item;
    }
}
