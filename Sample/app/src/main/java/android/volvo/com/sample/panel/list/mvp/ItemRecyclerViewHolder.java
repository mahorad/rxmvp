package android.volvo.com.sample.panel.list.mvp;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.volvo.com.sample.R;
import android.volvo.com.sample.panel.model.Item;
import android.widget.TextView;

public class ItemRecyclerViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public Item mItem;

        public ItemRecyclerViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = view.findViewById(R.id.id);
            mContentView = view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
